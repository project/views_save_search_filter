// (function($) {
//     var views_filter_url = window.location.pathname + window.location.search;
//     $('#edit-filter-url').val(34234);
//     console.log(324234 + views_filter_url);
// })(jQuery)


// jQuery(function(){
//     var views_save_search_filter_url = window.location.pathname + window.location.search;
//     jQuery('#edit-filter-url').val(views_save_search_filter_url);

//     console.log(324234);
// });

(function ($, Drupal) {
    Drupal.behaviors.myModuleBehavior = {
      attach: function (context, settings) {
		var view_id = getViewId();
        var views_save_search_filter_url = window.location.pathname + window.location.search;
        $('.filter_url').val(views_save_search_filter_url);
        //$('.filter_view_id').val(view_id);
		//appendViewIdToLink(view_id);
		
		$('.show_saved_filters').addClass('testing');
      }
    };
	
	function getViewId() {
	   var view_wrapper = $('div.view').attr('class');
	   var view_wrapper_clases = view_wrapper.split(" ");
	   var view_id_value = ""; 
	   $.each(view_wrapper_clases, function(i, clses) {
		if(clses.indexOf('view-id') != -1){
		   view_id_value = clses.replace("view-id-", "");
		}
	   });
	   return view_id_value;
	}
	
	function appendViewIdToLink(view_id) {
		var querystring = '?view_id='+view_id;
		var href = $('a.show_saved_filters').attr('href');
		if (href) {
		   $('.show_saved_filters').attr('href', href + querystring);
		}
	}

  })(jQuery, Drupal);