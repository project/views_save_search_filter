<?php

/**
 * @file
 * Install, update and uninstall functions for the views_save_search_filter module.
 */

use Drupal\views_save_search_filter\Form\ViewsSaveSearchFilterForm;

/**
 * Implements hook_schema().
 */
function views_save_search_filter_schema() {
  $schema['views_save_search_filter'] = [
    'description' => 'Table description.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique record ID.',
      ],
      'user_id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The uid of the user who created the record.',
      ],
      'username' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'User name',
      ],
      'access_type' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => ViewsSaveSearchFilterForm::ACCESS_TYPE_OWN,
        'description' => 'Whether filter is for all users or just for creator.',
      ],
      'view_id' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'View page id',
      ],
      'filter_title' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Save search title',
      ],
      'filter_url' => [
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'big',
        'description' => 'View search page url with querystring',
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Timestamp when the record was created.',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'user_id' => ['user_id'],
    ],
  ];

  return $schema;
}

/**
 * Adds 'access_type' field to the table.
 */
function views_save_search_filter_update_9001() {
  $access_field = [
    'type' => 'varchar_ascii',
    'length' => 255,
    'not null' => TRUE,
    'default' => ViewsSaveSearchFilterForm::ACCESS_TYPE_OWN,
    'description' => 'Whether filter is for all users or just for creator.',
  ];
  $schema = \Drupal::database()->schema();
  $schema->addField('views_save_search_filter', 'access_type', $access_field);
}
