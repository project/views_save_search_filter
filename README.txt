CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

This views save search filter module allows you to save your applied search on the views pages, logged in user can save his/her
search (search page URL), so next time they don't need to selelct same filter criteria, instead they can simply click on saved 
search links.


REQUIREMENTS
------------

This module requires the following modules:

 * Views (Drupal Core)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

You can place the below links in your desired position in your views page, may be in your views twig template or through views
ui, you can place it in the header section.

1. Below link opens a popup form allowing you to save your current search:
  <a class="use-ajax" data-dialog-options="{&quot;width&quot;:800}" data-dialog-type="modal" href="/views-save-search-filter/views-save-search-filter-form">Save this search</a>

2. Below link allow users to see their previosly saved search list, so they can click on it:
  <a class="use-ajax" data-dialog-options="{&quot;width&quot;:800}" data-dialog-type="modal" href="/views-save-search-filter/get-save-filters/">Show saved filter</a>
