<?php

namespace Drupal\views_save_search_filter\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a saved filters block.
 *
 * @Block(
 *   id = "here_views_save_filter_saved_filters",
 *   admin_label = @Translation("Save Filters Form"),
 *   category = @Translation("Custom")
 * )
 */
class ViewsSaveSearchFilterFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\views_save_search_filter\Form\ViewsSaveSearchFilterForm');
    return $form;
  }

}
