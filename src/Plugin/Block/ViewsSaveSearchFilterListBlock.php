<?php

namespace Drupal\views_save_search_filter\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\views_save_search_filter\Controller\ViewsSaveSearchFilterController;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "views_save_search_filter_list",
 *   admin_label = @Translation("Saved Filters List"),
 *   category = @Translation("views_save_search_filter")
 * )
 */
class ViewsSaveSearchFilterListBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build['content'] = [
      '#markup' => $this->getSaveFilters(),
    ];
    return $build;
  }

  /**
   * Get the list of saved filters.
   */
  public function getSaveFilters() {
    $here_save_cotrl = new ViewsSaveSearchFilterController();
    $titles = $here_save_cotrl->fetchAndPrepareSavedSearchLIst();
    return '<Ul class="filterlist">' . $titles . '</UL>';
    // Return $_SERVER['HTTP_HOST'] . " - " . $_SERVER['SCRIPT_URI'] . "-" . explode('?', $_SERVER['REQUEST_URI'])[0];.
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
