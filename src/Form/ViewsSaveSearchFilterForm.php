<?php

namespace Drupal\views_save_search_filter\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views_save_search_filter\Controller\ViewsSaveSearchFilterController;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a views_save_search_filter form.
 */
class ViewsSaveSearchFilterForm extends FormBase {

  /**
   * Search is accessible by all users.
   */
  const ACCESS_TYPE_ALL = 'all';

  /**
   * Search is accessible by the creator only.
   */
  const ACCESS_TYPE_OWN = 'own';

  /**
   * The Views save search filter controller class.
   *
   * @var Drupal\views_save_search_filter\Controller\ViewsSaveSearchFilterController
   */
  public $viewsSaveSearchController;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Constructs a ViewsSaveSearchFilterForm object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
    $this->viewsSaveSearchController = new ViewsSaveSearchFilterController($database);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'views_save_search_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $path = $_SERVER['REQUEST_URI'];
    $form['#prefix'] = '<div id="my_form_wrapper">';
    $form['#suffix'] = '</div>';
    $form['filter_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filter Title'),
      '#required' => TRUE,
      '#prefix' => '<div id="results">',
      '#suffix' => '</div>',
    ];
    $form['access_type'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make search accessible by other users.'),
    ];
    $form['view_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('View Id'),
      '#value' => $this->viewsSaveSearchController->getViewId(),
      '#required' => TRUE,
      '#attributes' => ['class' => ['filter_view_id']],
    ];
    $form['filter_url'] = [
      '#type' => 'hidden',
      '#title' => $this->t('filter url'),
      '#required' => TRUE,
      '#attributes' => ['class' => ['filter_url']],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#ajax' => [
        'callback' => '::processSubmit',
        'wrapper' => 'my_form_wrapper',
        'event' => 'click',
      ],
    ];
    $form['#attached']['library'][] = 'views_save_search_filter/views_save_search_filter';

    $value = $form_state->getValue('filter_title');
    if ($value !== NULL) {
      $form['filter_title']['#value'] = '';
      $this->messenger()->addStatus($this->t('Filter has been saved'));
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $filter_title = $form_state->getValue('filter_title');
    if (!empty($filter_title)) {
      $filter_url = $form_state->getValue('filter_url');
      $access_type = $form_state->getValue('access_type');
      $view_id = $form_state->getValue('view_id');
      $user_id = $this->currentUser()->id();
      $account = User::load($user_id);
      $username = $account->getAccountName();
      $this->database->insert('views_save_search_filter')
        ->fields([
          'filter_title' => $filter_title,
          'filter_url' => $filter_url,
          'view_id' => $view_id,
          'user_id' => $user_id,
          'username' => $username,
          'access_type' => $access_type == 1 ? static::ACCESS_TYPE_ALL : static::ACCESS_TYPE_OWN,
          'created' => time(),
        ])
        ->execute();
    }
  }

  /**
   * Process submit form using ajax callback.
   */
  public function processSubmit(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand());

    return $response;
  }

  /**
   * Get the list of saved filters.
   */
  public function getSaveFilters() {
    $titles = $this->viewsSaveSearchController->fetchAndPrepareSavedSearchLIst();
    return [
      '#markup' => '<Ul class="filterlist">' . $titles . '</UL>',
    ];
  }

}
