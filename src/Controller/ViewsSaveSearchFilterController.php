<?php

namespace Drupal\views_save_search_filter\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\views_save_search_filter\Form\ViewsSaveSearchFilterForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for views_save_search_filter routes.
 */
class ViewsSaveSearchFilterController extends ControllerBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Constructs a ViewsSaveSearchFilterController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Get the list of saved views filter by logged in user id and current page view id.
   */
  public function getSaveFilters() {
    $titles = $this->fetchAndPrepareSavedSearchLIst();
    return [
      '#markup' => '<Ul class="filterlist">' . $titles . '</UL>',
    ];
  }

  /**
   * Fetches the saved search list.
   */
  public function fetchAndPrepareSavedSearchLIst() {
    $view_id = $this->getViewId();
    $user_id = $this->currentUser()->id();
    $fields = ['id', 'user_id', 'filter_title', 'filter_url', 'view_id'];
    $query = $this->database->select('views_save_search_filter', 'vssf')
      ->condition('vssf.view_id', $view_id)
      ->fields('vssf', $fields)
      ->range(0, 10);
    $condition_group = $query->orConditionGroup()
      ->condition('vssf.user_id', $user_id)
      ->condition('vssf.access_type', ViewsSaveSearchFilterForm::ACCESS_TYPE_ALL);
    $query->condition($condition_group);
    $result = $query->execute();
    if (empty($result)) {
      return $this->t('No item is saved yet!');
    }
    $titles = '';
    foreach ($result as $record) {
      $items = [
        "<a target='_blank' href='" . $record->filter_url . "'>" . $record->filter_title . "</a>",
      ];
      if ($record->user_id == $user_id) {
        $items[] = "<a title='Delete' data-dialog-options='{&quot;width&quot;:400}' data-dialog-type='modal' class='use-ajax' href='/views-save-search-filter/delete-save-filters/" . $record->id . "' onclick='return confirm(\"Are you sure you want to delete this item?\");'><i class='fa fa-trash-o fa-fw'></i>" . $this->t('Delete') . "</a>";
      }
      $titles .= '<li>' . implode(' | ', $items) . '</li>';
    }

    return $titles;
  }

  /**
   * Delete the saved filters by filter id.
   */
  public function deleteSaveFilters(int $filter_id) {
    if (!empty($filter_id)) {
      $user_id = $this->currentUser()->id();
      $query = $this->database->delete('views_save_search_filter')
        ->condition('user_id', $user_id, '=')
        ->condition('id', $filter_id, '=');
      $query->execute();

      $response = new AjaxResponse();
      $selector = '.filterlist';
      $content = $this->getSaveFilters();
      $settings = []; /*An array of JavaScript settings to be passed to any attached behaviors.*/
      $response->addCommand(new ReplaceCommand($selector, $content, $settings));
      return $response;
    }
  }

  /**
   * Get the view or page URL to store the save search against it.
   */
  public function getViewId() {
    $path = $_SERVER['HTTP_REFERER'];
    $req_uri_with_noparam = strtok($path, '?');
    /*
    $view_id = explode('/', $req_uri_with_noparam);
    $count_arr = count($view_id);
    if($count_arr > 4) {
    $view_id = $view_id[4];
    } else {
    $view_id = $view_id[3];
    }
    $view_id = str_replace("-", "_", $view_id);
    return $view_id;
     */
    return $req_uri_with_noparam;
  }

}
